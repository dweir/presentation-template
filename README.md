# David Weir's template for scientific presentations

Dependencies (as submodules):

* [reveal.js](http://lab.hakim.se/reveal-js/)
* [MathJax](https://www.mathjax.org/)
* [Font Awesome](http://www.fontawesome.io/)

You may prefer to use MathJax from a CDN. It's quite big, even as a shallow clone. This just consists of changing the location of `MathJax.js` in the `initialize()` routine at the bottom of `index.html`.

## Other notes/issues:

* See the [reveal.js documentation](https://github.com/hakimel/reveal.js/blob/master/README.md) for information on printing/PDF export, speaker notes, and multiplexing. Multiplexing in particular is very cool.
* I recommend forking and self-hosting a separate repo for every presentation you make. If you have a better idea, please let me know!

### Quick help guide (mostly git stuff):

1. To 'get' the submodules:

         git submodule init
         git submodule update

2. Pruning history to make it a fresh repo, but keep submodules ([source](https://stackoverflow.com/a/13102849)):

         git checkout --orphan newBranch
         git add -A  # Add all files and commit them
         git commit
         git branch -D main  # Deletes the main branch
         git branch -m main  # Rename the current branch to main
         git gc --aggressive --prune=all     # remove the old files


### Pushing to non-bare repo on server ###

On server, make new repo:

         git init

Make sure .git/config has:

         [receive]
	         denyCurrentBranch = ignore


Make sure .git/hooks/post-receive is executable and contains:

         THISFILE="$(pwd)/$0"
         REPODIR=$(dirname $(dirname $(dirname "$THISFILE")))
         # Notify the user that we are updating HEAD.
         echo -e "\033[32m"
         echo "Updating HEAD for you."
         echo git --git-dir "$REPODIR/.git" --work-tree "$REPODIR/" reset --hard main
         echo -e "\033[0m"
         git --git-dir "$REPODIR/.git" --work-tree "$REPODIR/" reset --hard main

Also the following if using a symlinked set of modules (should do the
same for common media?):

         echo "Restoring symlinks to submodules"
         rm -rf $REPODIR/reveal.js
         ln -s $REPODIR/../reveal.js/ $REPODIR/reveal.js
         rm -rf $REPODIR/MathJax
         ln -s $REPODIR/../MathJax/ $REPODIR/MathJax
         rm -rf $REPODIR/Font-Awesome
         ln -s $REPODIR/../Font-Awesome/ $REPODIR/Font-Awesome
